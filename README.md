#Convert output of `golang.org/x/tools/cmd/callgraph` to `dot` graph.
 
Install `callgraph`:
 
`go get golang.org/x/tools/cmd/digraph`
 
Analyze code:

`GOPATH=/path/to/geth callgraph -algo=static github.com/ethereum/go-ethereum/cmd/geth > callgraph`

Generate graph:

`cat callgraph | callviz '(*github.com/ethereum/go-ethereum/core/vm.EVM).Call' > vmcall.dot`

Show graph:

`xdot vmcall.dot`
 
![screenshot](https://bytebucket.org/yushi/golang-callgraph-vizualization/raw/da2223a46d1b7f81081510d51845f906b2f5c1d3/vmcall.jpg)