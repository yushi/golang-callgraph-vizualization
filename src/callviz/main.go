package main

import (
	"bufio"
	"fmt"
	"os"
    "regexp"
)

type  CallGraph map[string] ([]string) //alt: container/list


func buildCallgraph () (CallGraph) {
    callGraph := make(CallGraph)
	scanner := bufio.NewScanner(os.Stdin)
    re := regexp.MustCompile(`^(\S+)\s*--.+-->\s*(\S+)$`)
    lineNo := 1

	for scanner.Scan() {
        line := scanner.Text()

        m := re.FindStringSubmatch(line)
        if m == nil {
            fmt.Fprintf(os.Stderr, "can't parse line %d: %s\n", lineNo, line)
            os.Exit(1)
        }
        caller := m[1]
        callee := m[2]

        callGraph[callee] = append(callGraph[callee], caller)

        lineNo += 1
	}
	if err := scanner.Err(); err != nil {
		fmt.Fprintln(os.Stderr, "reading standard input:", err)
        os.Exit(1);
	}
    return callGraph
}

func printCallerRecur (callee string, callGraph CallGraph) {
     for _, caller := range callGraph[callee] {
         fmt.Printf("\"%s\" -> \"%s\";\n", caller, callee)
         printCallerRecur(caller, callGraph);
     }
}

func main () {
     callGraph := buildCallgraph()
     fmt.Printf("digraph {\n")
     printCallerRecur(os.Args[1], callGraph)
     fmt.Printf("}\n")
}
